PROJECT_LIST_SERVICES += adminer

ADMINER_COMPOSE_FILE ?= $(RESOURCES)/adminer/docker-compose
COMPOSE_FILE += -f $(ADMINER_COMPOSE_FILE).yml -f $(ADMINER_COMPOSE_FILE).$(ENV).yml
ADMINER_DEFAULT_SERVER ?= db
PRE_TARGETS_ENV += adminer-env
TARGETS_HELP += adminer-help
PRE_TARGETS_UP += adminer-up
PRE_TARGETS_START += adminer-start
PRE_TARGETS_STOP += adminer-stop
PRE_TARGETS_DOWN += adminer-down

adminer-help:
	$(call SHOW_TITLE_HELP, ADMINER)
	$(call SHOW_CMD_HELP, adminer-up) Запускаем окружение UP \(Adminer\)
	$(call SHOW_CMD_HELP, adminer-start) Запускаем/стартуем остановленное приложение \(Adminer\)
	$(call SHOW_CMD_HELP, adminer-stop) Остановлеваем приложение \(Adminer\)
	$(call SHOW_CMD_HELP, adminer-rm) удаляем остановленное приложение \(Adminer\)
	$(call SHOW_CMD_HELP, adminer-clean) очищаем окружение

adminer-up:
	@docker-compose -f $(COMPOSE_FILE) up -d adminer

adminer-start:
	@docker-compose -f $(COMPOSE_FILE) start adminer

adminer-stop:
	@docker-compose -f $(COMPOSE_FILE) stop adminer

adminer-rm:
	@docker-compose -f $(COMPOSE_FILE) rm -s adminer

adminer-down: adminer-stop adminer-rm

adminer-clean:

adminer-env:
	@echo "# ---- ADMINER ----" >> .env
	@echo "ADMINER_DEFAULT_SERVER="$(ADMINER_DEFAULT_SERVER) >> .env
	@echo "" >> .env
